/**
 * @file   xor_crypt.c
 * @author Aron Ahmadia <aron@ahmadia.net>
 * @date   Sat Oct 22 14:20:32 2011
 * 
 * @brief  very simple function for xor-crypting
 * 
 * 
 */

void xor_crypt(char* crypt_block, int num_bytes) 
{
  char key[8]="flowers";
  int i;

  for (i=0; i<num_bytes;i++) {
    crypt_block[i] = crypt_block[i] ^ key[i%8];
  }
}
