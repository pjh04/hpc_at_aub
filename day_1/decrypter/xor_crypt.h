/**
 * @file   xor_crypt.h
 * @author Aron Ahmadia <aron@ahmadia.net>
 * @date   Sat Oct 22 14:20:13 2011
 * 
 * @brief  header file for xor_crypt (public domain)
 * 
 * 
 */


void xor_crypt(char* crypt_block, int num_bytes);
