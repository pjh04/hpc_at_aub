/**
 * @file   decrypter.c
 * @author Aron Ahmadia <aron@ahmadia.net
 * @date   Sat Oct 22 11:18:16 2011
 * 
 * @brief  Simple (broken :D) decrypter, this code is public domain
 * 
 * 
 */

#include <stdio.h>
#include "xor_crypt.h"

#define BLOCKSIZE 64

int main(int argc, char *argv[]) {
{
  int i, num_bytes, not_end;
  char crypt_block[BLOCKSIZE];

  num_bytes = fread(crypt_block, sizeof(char), BLOCKSIZE, stdin); 

  while (num_bytes) {

    xor_crypt(crypt_block, num_bytes);

    fwrite(crypt_block, sizeof(char), num_bytes, stdout);

    num_bytes = fread(crypt_block, sizeof(char), BLOCKSIZE, stdin); 
  }

  return 0;
}
